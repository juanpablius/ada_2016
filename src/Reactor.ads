with Ada.Real_Time;
with Ada.Real_Time.Timing_Events;
with Ada.Text_IO;

use Ada.Real_Time;
use Ada.Real_Time.Timing_Events;
use Ada.Text_IO;

with Ada.Calendar.Formatting;

package Reactor is

   subtype RangoDeTemperatura is Integer range 0..10000;

   protected type UnReactor is

  --   pragma Interrupt_Priority(System.Interrupt_Priority'Last);

     procedure iniciarSensor;
     procedure iniciarActuador;
     procedure Timer(event: in out Ada.Real_Time.Timing_Events.Timing_Event);
     entry leer(dato:out Integer; re:Integer);
     procedure abrirCompuerta(dato:Integer; re:Integer);
      procedure cerrarCompuerta;
      procedure calentarReactor;
      function getTemperaturaReactor return RangoDeTemperatura;


   private

      nextTime:Ada.Real_Time.Time;
      	 Temperatura : RangoDeTemperatura := 1450;
     	 TempInicial1:Integer:=1450;
 	 TempInicial2:Integer:=1450;
      	 TempInicial3:Integer:=1450;

   --hacer un if para saber que temIni es
      	 datoDisponible:Boolean:=True;

   	entradaJitterControl:Ada.Real_Time.Timing_Events.Timing_Event;
     --500ms  del input jitter-- esto es para el iniciar
      	entradaPeriodo:Ada.Real_Time.Time_Span:=Ada.Real_Time.Milliseconds(500);
        entradaPeriodo1:Ada.Real_Time.Time_Span:=Ada.Real_Time.Milliseconds(2000);
   	entradaPeriodo2:Ada.Real_Time.Time_Span:=Ada.Real_Time.Milliseconds(100);

   	salidaJitterControl:Ada.Real_Time.Timing_Events.Timing_Event;
     --300ms -40 ms del output jitter
       salidaPeriodo:Ada.Real_Time.Time_Span:=Ada.Real_Time.Milliseconds(100);
       salidaPeriodo1:Ada.Real_Time.Time_Span:=Ada.Real_Time.Milliseconds(1000);

   end UnReactor;


end Reactor;
