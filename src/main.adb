with Ada.Text_IO;
with Ada.Real_Time;
with Ada.Calendar;
with Ada.Real_Time.Timing_Events;
with System;
use Ada.Calendar;
use Ada.Real_Time;
use Ada.Text_IO;


use Ada.Real_Time.Timing_Events;
with Reactor;
use Reactor;
with Ada.Numerics.Discrete_Random;
with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;


procedure Main is

--	ELECCION DEL REACTOR ALEATORIO

    subtype reactorAleatorio is Integer range 1..3;
    package Aleatorio is new Ada.Numerics.Discrete_Random(reactorAleatorio);
    subtype numeroReactor    is Integer range 1..3;       -- N�mero


-----------CENTRAL NUCLEAR ---------------------------
   protected type centralNuclear is
      procedure seleccionarReactor;
      function reactorSeleccionado return numeroReactor;
   private
      nR:numeroReactor;

   end centralNuclear;

-----------------------------------------------------
   protected body centralNuclear is

      procedure seleccionarReactor is
         rA:reactorAleatorio;
         seed:Aleatorio.Generator;

      begin
         aleatorio.Reset(seed);
         rA :=Aleatorio.Random(seed);
         nR := rA;
	 --Ada.Text_IO.Put_Line("Reactor elegido "&Integer'Image(nR));--debug
      exception
            when Constraint_Error => null;
      end;

      function reactorSeleccionado return numeroReactor is
      begin
         return nR;
      end reactorSeleccionado;


   end centralNuclear;


   ---------------------------------------------
   cN : centralNuclear;

   ----------------------------------------------
   -- UN OBJETO REACTOR POR CADA REACTOR
   rc1: aliased unReactor;
   rc2: aliased unReactor;
   rc3: aliased unReactor;



   task type incrementarTemperatura;
   task body incrementarTemperatura is -- Esta tarea se invocar� en un bucle infinito, cada 2 sec
      ahora :Ada.Real_Time.Time;
      espera2sec:Time_span:=Milliseconds(2000);

   begin
      ahora:= Clock + espera2sec;
      loop

         cN.seleccionarReactor;
         delay until ahora;
         ahora := ahora + espera2sec;
      end loop;

   end incrementarTemperatura;

   incTemp : incrementarTemperatura;


   procedure getTemperatura (calor1:out rangoDeTemperatura; calor2:out rangoDeTemperatura; calor3:out rangoDeTemperatura) is

      task type t1;
      task body t1 is
      begin
         calor1:=rc1.getTemperaturaReactor;
      end t1;

      task type t2;
      task body t2 is
      begin
         calor2:=rc2.getTemperaturaReactor;
      end t2;

      task type t3;
      task body t3 is
      begin
         calor3:=rc3.getTemperaturaReactor;
      end t3;

      accesoaRC1:t1;
      accesoaRC2:t2;
      accesoaRC3:t3;
   begin
      null;

   end getTemperatura;

--------------------------------------------------

   procedure getTemperaturaRC1(calor:out rangoDeTemperatura) is

      task type t1;
      task body t1 is
      begin
         calor:=rc1.getTemperaturaReactor;
      end t1;

      accesoaRC1:t1;

     begin
      null;

   end getTemperaturaRC1;

   procedure getTemperaturaRC2(calor:out rangoDeTemperatura) is

      task type t2;
      task body t2 is
      begin
         calor:=rc2.getTemperaturaReactor;
      end t2;

      accesoaRC2:t2;

     begin
      null;

   end getTemperaturaRC2;


    procedure getTemperaturaRC3(calor:out rangoDeTemperatura) is

      task type t3;
      task body t3 is
      begin
         calor:=rc3.getTemperaturaReactor;
      end t3;

      accesoaRC3:t3;

     begin
      null;

   end getTemperaturaRC3;


   ---------------------------------------

   task type CoordinadorRC1;
   task body CoordinadorRC1 is

      salidaRC1 : rangoDeTemperatura;
      entradaRC1 : rangoDeTemperatura;
      -------------------------------------
      type tipoDeReactor is access all UnReactor;

      reactorDeHomer:tipoDeReactor:=rc1'access;

      ---------------------------------------
   begin
         entradaRC1 := 9000; -- 2000

      loop

         delay 0.2;
         getTemperaturaRC1(salidaRC1);

         Ada.Text_IO.Put_Line("Coordinador - Reactor 1 : OK  - Temperatura "&Integer'Image(salidaRC1));-- MENSAJE QUE MUESTRA QUE EL REACTOR FUNCIONA CORRECTAMENTE


         -- rc1.leer(salidaRC1,1); MODO JOSE

         loop

            -------------------------------------------------------------------
            if (salidaRC1 < 1500) then
               null; -- NoNada

            elsif ((salidaRC1 >=1500) and (salidaRC1 <=1750))  then

               rc1.abrirCompuerta(salidaRC1,1 );
               getTemperaturaRC1(entradaRC1);

            elsif  (salidaRC1 > 1750) then

               rc1.abrirCompuerta(salidaRC1,1);
               getTemperaturaRC1(entradaRC1);
               Ada.Text_IO.Put_Line("ALERTA - Reactor 1 - Id: 1 - Temperatura : "&Integer'Image(salidaRC1));


            end if;
	    -----------------------------------------------------------------
            if (entradaRC1 < 1500) then
               --Ada.Text_IO.Put_Line("cerrar puta compuerta");
               rc1.cerrarCompuerta;
               entradaRC1 := 2000;
            end if;
            ---------------------------------------------------------------
            getTemperaturaRC1(salidaRC1); -- Actulizar el valor de la temperatura




         exit when salidaRC1 < 1500;
         end loop;

      end loop;


   end CoordinadorRC1;


   task type CoordinadorRC2;
   task body CoordinadorRC2 is

      salidaRC2 : rangoDeTemperatura;
      entradaRC2 : rangoDeTemperatura;
      -------------------------------------
      type tipoDeReactor is access all UnReactor;

      reactorDeCarl:tipoDeReactor:=rc2'access;

      ---------------------------------------
   begin
         entradaRC2 := 9000; -- 2000

      loop

         delay 0.2;
         getTemperaturaRC2(salidaRC2);

         Ada.Text_IO.Put_Line("Coordinador - Reactor 2 : OK  - Temperatura "&Integer'Image(salidaRC2));-- MENSAJE QUE MUESTRA QUE EL REACTOR FUNCIONA CORRECTAMENTE


         -- rc1.leer(salidaRC1,1); MODO JOSE

         loop

            -------------------------------------------------------------------
            if (salidaRC2 < 1500) then
               null; -- NoNada

            elsif ((salidaRC2 >=1500) and (salidaRC2 <=1750))  then

               rc2.abrirCompuerta(salidaRC2,2 );
               getTemperaturaRC2(entradaRC2);

            elsif  (salidaRC2 > 1750) then

               rc1.abrirCompuerta(salidaRC2,2);
               getTemperaturaRC2(entradaRC2);
               Ada.Text_IO.Put_Line("ALERTA - Reactor 2 - Id: 1 - Temperatura : "&Integer'Image(salidaRC2));


            end if;
	    -----------------------------------------------------------------
            if (entradaRC2 < 1500) then
               --Ada.Text_IO.Put_Line("cerrar puta compuerta");
               rc2.cerrarCompuerta;
               entradaRC2 := 2000;
            end if;
            ---------------------------------------------------------------
            getTemperaturaRC2(salidaRC2); -- Actulizar el valor de la temperatura




         exit when salidaRC2 < 1500;
         end loop;

      end loop;


   end CoordinadorRC2;


    task type CoordinadorRC3;
   task body CoordinadorRC3 is

      salidaRC3 : rangoDeTemperatura;
      entradaRC3 : rangoDeTemperatura;
      -------------------------------------
      type tipoDeReactor is access all UnReactor;

      reactorDeLenny:tipoDeReactor:=rc3'access;

      ---------------------------------------
   begin
         entradaRC3 := 9000; -- 2000

      loop

         delay 0.2;
         getTemperaturaRC3(salidaRC3);

         Ada.Text_IO.Put_Line("Coordinador - Reactor 3 : OK  - Temperatura "&Integer'Image(salidaRC3));-- MENSAJE QUE MUESTRA QUE EL REACTOR FUNCIONA CORRECTAMENTE


         -- rc1.leer(salidaRC1,1); MODO JOSE

         loop

            -------------------------------------------------------------------
            if (salidaRC3 < 1500) then
               null; -- NoNada

            elsif ((salidaRC3 >=1500) and (salidaRC3 <=1750))  then

               rc3.abrirCompuerta(salidaRC3,3 );
               getTemperaturaRC3(entradaRC3);

            elsif  (salidaRC3 > 1750) then

               rc3.abrirCompuerta(salidaRC3,3);
               getTemperaturaRC3(entradaRC3);
               Ada.Text_IO.Put_Line("ALERTA - Reactor 3 - Id: 3 - Temperatura : "&Integer'Image(salidaRC3));


            end if;
	    -----------------------------------------------------------------
            if (entradaRC3 < 1500) then
               --Ada.Text_IO.Put_Line("cerrar puta compuerta");
               rc3.cerrarCompuerta;
               entradaRC3 := 2000;
            end if;
            ---------------------------------------------------------------
            getTemperaturaRC3(salidaRC3); -- Actulizar el valor de la temperatura




         exit when salidaRC3< 1500;
         end loop;

      end loop;


   end CoordinadorRC3;




   tareaRC1 : CoordinadorRC1;
   tareaRC2 : CoordinadorRC2;
   tareaRC3 : CoordinadorRC3;
   task type funcionamiento;

   task body funcionamiento is

       disponible:Boolean:=True;
      varAux : rangoDeTemperatura:=0;
      --varitemp1 : rangoDeTemperatura;
      --varitemp2 : rangoDeTemperatura;
      --varitemp3 : rangoDeTemperatura;

      type tipoReactor is access all UnReactor;

      reactorHomer:tipoReactor:=rc1'access;
      reactorCarl:tipoReactor:=rc2'access;
      reactorLenny:tipoReactor:=rc3'access;

   begin

      loop
         delay 0.1;

         if (varAux /= cN.reactorSeleccionado) then  -- Flag de bucle

            varAux:=cN.reactorSeleccionado;

            if (cN.reactorSeleccionado = 1 ) then
               Ada.Text_IO.Put_Line("Aumento en el reactor de Homer - Reactor :"&Integer'Image(cN.reactorSeleccionado));-- debug
               --Ada.Text_IO.Put_Line("Temperatura Antes "&Integer'Image(reactorHomer.getTemperaturaReactor));
               reactorHomer.calentarReactor;
	       --Ada.Text_IO.Put_Line("Temperatura Despues "&Integer'Image(reactorHomer.getTemperaturaReactor));

  	    end if;

  	    if (cN.reactorSeleccionado = 2 ) then
               Ada.Text_IO.Put_Line("Aumento en el Reactor de Carl - Reactor :"&Integer'Image(cN.reactorSeleccionado));--debug
               -- Ada.Text_IO.Put_Line("Temperatura Antes "&Integer'Image(reactorCarl.getTemperaturaReactor));
               reactorCarl.calentarReactor;
	       --Ada.Text_IO.Put_Line("Temperatura Despues "&Integer'Image(reactorCarl.getTemperaturaReactor));
  	    end if;

  	    if (cN.reactorSeleccionado = 3 ) then
               Ada.Text_IO.Put_Line("Aumento en Reactor de Lenny - Reactor :"&Integer'Image(cN.reactorSeleccionado));--debug
              --  Ada.Text_IO.Put_Line("Temperatura Antes "&Integer'Image(reactorLenny.getTemperaturaReactor));
               reactorLenny.calentarReactor;
	       --Ada.Text_IO.Put_Line("Temperatura Despues "&Integer'Image(reactorLenny.getTemperaturaReactor));
  	     end if;

         end if;
      end loop;

   end funcionamiento;

   F: funcionamiento;



   --LLAMADAS A LAS TAREAS PARA CREAR LOS HILOS --





begin -- main

null;


end Main;
  --envio de 1,2,3 vivo y 0 muerto
