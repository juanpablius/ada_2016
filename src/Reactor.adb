package body Reactor is
   protected body UnReactor is

      procedure iniciarSensor is

         begin
         datoDisponible:=False;
         nextTime:=Clock+entradaPeriodo;
         Ada.Real_Time.Timing_Events.Set_Handler(entradaJitterControl, nextTime, Timer'Access);

      end iniciarSensor;

      procedure iniciarActuador is
       begin
         null;
         --he comentado en el main esperar los 260ms
         nextTime:=Clock+salidaPeriodo;
      end iniciarActuador;

      entry leer(dato:out Integer; re:Integer)
        when datoDisponible is
      begin

         nextTime:=nextTime+entradaPeriodo2;
         Ada.Real_Time.Timing_Events.Set_Handler(entradaJitterControl, nextTime, Timer'Access);
         -- hacer un if para coger la tempIni correcta

          if re =1 then

            dato:=Temperatura;
              Ada.Text_IO.Put_Line("Leyendo Reactor1=> temperatura "&Integer'Image(dato));
         elsif re =2 then
            dato:=Temperatura;
              Ada.Text_IO.Put_Line("Leyendo Reactor2=> temperatura  "&Integer'Image(dato));
         else
            dato:=Temperatura;
              Ada.Text_IO.Put_Line("Leyendo Reactor3=> temperatura  "&Integer'Image(dato));
         end if;

         datoDisponible:=True;


      end leer;

      procedure Timer(event:in out Ada.Real_Time.Timing_Events.Timing_Event) is
      begin
         --un segundo por cada 50 grados AQUI ES DONDE SE RESTAN LOS 50� CADA SEGUNDO LLAMANDO A VARIABLES
	     nextTime:=nextTime+salidaPeriodo1;
         Ada.Real_Time.Timing_Events.Set_Handler(salidaJitterControl, nextTime, Timer'Access);
      end Timer;

      procedure abrirCompuerta(dato:Integer; re:Integer) is
      begin
          nextTime:=Clock+salidaPeriodo;
         Ada.Real_Time.Timing_Events.Set_Handler(salidaJitterControl, nextTime, Timer'Access);
          Ada.Text_IO.Put_Line("Puerta abierta");


        -- loop
         nextTime:=nextTime+salidaPeriodo1;
         Ada.Real_Time.Timing_Events.Set_Handler(salidaJitterControl, nextTime, Timer'Access);

         if re =1 then
               temperatura:=temperatura-50;
                Ada.Text_IO.Put_Line("Puerta abierta--->Reactor1=> temperatura  "&Integer'Image(temperatura));


         elsif re =2 then
               temperatura:=temperatura-50;
                Ada.Text_IO.Put_Line("Puerta abierta--->Reactor2=> temperatura  "&Integer'Image(temperatura));

         else
               temperatura:=temperatura-50;
                Ada.Text_IO.Put_Line("Puerta abierta--->Reactor3=> temperatura  "&Integer'Image(temperatura));

            end if;
       --     exit  when TempInicial1<=1500 or  TempInicial2<=1500 or  TempInicial3<=1500;
         --end loop;

      end abrirCompuerta;

      procedure cerrarCompuerta is
      begin

         nextTime:=Clock+salidaPeriodo;
         Ada.Real_Time.Timing_Events.Set_Handler(salidaJitterControl, nextTime, Timer'Access);
          Ada.Text_IO.Put_Line("Puerta cerrada");
      end cerrarCompuerta;

      procedure calentarReactor is
      begin
      	temperatura:= temperatura + 150;
      end calentarReactor;

      function getTemperaturaReactor return RangoDeTemperatura is
      begin
         return Temperatura;
      end;



   end unReactor;

end Reactor;
